//
//  StreamClient.swift
//  RadiatorPluginSDK
//
//  Created by Chris Pastl on 03.07.23.
//  Copyright © 2023 crispybits.app. All rights reserved.
//

import Foundation

open class StreamClient {
    
    public enum Error: Swift.Error {
        case failed(String?)
    }
    
    private var outPipe: Pipe?
    private var errPipe: Pipe?
    
    public final var streamPipe: Pipe { return makePipes() }
    
    
    public init() {
        
    }
    
    
    public final func makePipes() -> Pipe {
        outPipe = Pipe()
        errPipe = Pipe()
        return outPipe!
    }
    
    public final func stream(shCommand: String, callback: @escaping RadiatorPluginStreamCallback) throws {
        guard let outPipe, let errPipe else { fatalError() }
        var terminationReason: String?
        let process = Process()
        process.launchPath = "/bin/sh"
        process.arguments  = ["-c", shCommand]
        process.standardOutput = outPipe
        process.standardError  = errPipe
        
        errPipe.fileHandleForReading.readabilityHandler = { fh in
            guard let str = String(data: fh.availableData, encoding: .utf8) else { return }
            NSLog("StreamClient: %@", str)
            if str.localizedCaseInsensitiveContains("error") {
                terminationReason = str
                process.terminate()
            }
        }
        
        process.terminationHandler = { proc in
            outPipe.fileHandleForReading.readabilityHandler = nil
            errPipe.fileHandleForReading.readabilityHandler = nil
        }
        
        process.launch()
        process.waitUntilExit()
        
        guard process.terminationStatus == 0 else { throw Error.failed(terminationReason) }
        callback(.done)
    }
}


extension StreamClient.Error: LocalizedError {
    public var errorDescription: String? {
        switch self {
            case .failed(let reason): return reason
        }
    }
}
