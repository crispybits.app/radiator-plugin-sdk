//
//  HTTPClient.swift
//  RadiatorPluginSDK
//
//  Created by Chris Pastl on 03.07.23.
//  Copyright © 2023 crispybits.app. All rights reserved.
//

import Foundation

public class HTTPClient {
    
    public enum Error: Swift.Error {
        case invalidURLReponseType
        case badStatusCode(Int)
    }
    
    
    public init() {}

    
    public func fetch(_ url: URL) async throws -> Data {
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        request.timeoutInterval = 10
        let (data, response) = try await URLSession.shared.data(for: request)
        guard let httpResponse = response as? HTTPURLResponse else { throw Error.invalidURLReponseType }
        guard httpResponse.statusCode == 200 else { throw Error.badStatusCode(httpResponse.statusCode) }
        return data
    }
}


public extension String {
    var unescaped: String {
        let chars = [
            "&amp;": "&",
            "&lt;": "<",
            "&gt;": ">",
            "&quot;": "\"",
            "&apos;": "'"
        ]
        var str = self
        for (esc, unesc) in chars {
            str = str.replacingOccurrences(of: esc, with: unesc, options: .literal)
        }
        return str
    }
}
