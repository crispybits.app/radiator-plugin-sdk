//
//  SearchClient.swift
//  RadiatorPluginSDK
//
//  Created by Chris Pastl on 03.07.23.
//  Copyright © 2023 crispybits.app. All rights reserved.
//

import Foundation

open class SearchClient {
    
    public static let defaultMaxSearchResults = 30
    public var maxSearchResults: Int { SearchClient.defaultMaxSearchResults }
    
    public init() {
        
    }
    
}


public extension TimeInterval {
    init(string: String) {
        var time: TimeInterval = 0
        let parts = string.components(separatedBy: ":")
        for (idx, part) in parts.reversed().enumerated() {
            time += (Double(part) ?? 0) * pow(60.0, Double(idx))
        }
        self = time
    }
}
