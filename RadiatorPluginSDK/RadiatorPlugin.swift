//
//  RadiatorPlugin.swift
//  RadiatorPluginSDK
//
//  Created by Chris Pastl on 03.07.23.
//  Copyright © 2023 crispybits.app. All rights reserved.
//

import Cocoa

public protocol RadiatorPlugin {
    
    init()
    
    var id: Int { get }
    var name: String { get }
    var icon: NSImage { get }
    var color: NSColor { get }
    var columns: [String]? { get }
    
    var streamPipe: Pipe { get }
    
    func search(query: String) -> AsyncThrowingStream<RadiatorPluginSearchItem, Error>
    func stream(url: URL, callback: @escaping RadiatorPluginStreamCallback) throws
}



public struct RadiatorPluginSearchItem {
    public let title: String
    public let url: URL
    public let time: TimeInterval
    public let artwork: Data?
    
    public init(title: String, url: URL, time: TimeInterval, artwork: Data?) {
        self.title = title
        self.url = url
        self.time = time
        self.artwork = artwork
    }
}



public typealias RadiatorPluginStreamCallback = (RadiatorPluginStreamState) -> Void

public enum RadiatorPluginStreamState {
    case receive(Data)
    case done
    case error(Error)
}
