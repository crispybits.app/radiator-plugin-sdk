//
//  RadiatorPluginSDK.h
//  RadiatorPluginSDK
//
//  Created by Chris Pastl on 03.07.23.
//  Copyright © 2023 crispybits.app. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for RadiatorPluginSDK.
FOUNDATION_EXPORT double RadiatorPluginSDKVersionNumber;

//! Project version string for RadiatorPluginSDK.
FOUNDATION_EXPORT const unsigned char RadiatorPluginSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <RadiatorPluginSDK/PublicHeader.h>


